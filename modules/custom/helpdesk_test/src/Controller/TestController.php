<?php

namespace Drupal\helpdesk_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\helpdesk_complaint\Repository\ComplaintRepo;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\user\Entity\User;

/**
 * Defines TestController class.
 */
class TestController extends ControllerBase {

  public function content() {
    $now = date('Y-m');
    for($x = 11; $x >= 0; $x--) {
      $date = date('Y-m', strtotime($now . " -$x month"));
      $from = date('Y-m-01\T00:00:00', strtotime($date));
      $until = date('Y-m-t\T23:59:59', strtotime($date));
      $in_time = ComplaintRepo::ComplaintGetAll(
        array(
          'created_from' => $from,
          'created_until' => $until,
          'missed_deadline' => "0",
          'count' => true
        )
      );

      $complaint_data = array('in_time' => ($in_time == null ? "0" : $in_time['count'].''), 'date' => $date);

      $data['deadline_complaint_report'][] = $complaint_data;
    }
    return new JsonResponse($data);
  }

}
