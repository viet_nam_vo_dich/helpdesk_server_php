<?php

namespace Drupal\helpdesk_complaint\Repository;

use Drupal;
use DateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * trait ComplaintRepo
 *
 * @package Drupal\helpdesk_complaint\Controller
 */
trait ComplaintRepo {

  /**
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *
   * @param $options
   *
   * @return array
   * @throws \Exception
   */
  public static function ComplaintGetAll($options = array()) {
    $query = Drupal::entityQuery('node')
      ->condition('type', 'complaint', '=')
      ->sort('field_priority', 'DESC');

    self::ComplaintAlterQuery($query, $options);

    if($options['count'] != null && $options['missed_deadline'] == null) {
      $count = $query->count()->execute();
      return array('count' => $count);
    } else {
      $nids = $query->execute();

      return self::ComplaintGetAllDataProcess($nids, $options);
    }
  }

  /**
   * @param $query
   *
   * @param $options
   *
   * @throws \Exception
   */
  public static function ComplaintAlterQuery(&$query, $options) {
    if($options['status'] != null) {
      $query->condition('field_status', $options['status'], 'IN');
    }

    if($options['category'] != null) {
      $query->condition('field_category', $options['category'], 'IN');
    }

    if($options['technician'] != null) {
      $query->condition('field_technician', $options['technician'], 'IN');
    }

    if($options['priority'] != null) {
      $query->condition('field_priority', $options['priority'], '=');
    }

    if($options['department'] != null) {
      $query->condition('field_department', $options['department'], '=');
    }

    if($options['created_from'] != null) {
      $options['created_from'] = new DateTime($options['created_from']);
      $query->condition('created', $options['created_from']->format(DateTimeItemInterface::STORAGE_TIMEZONE), '>=');
    }

    if($options['created_until'] != null) {
      $options['created_until'] = new DateTime($options['created_until']);
      $query->condition('created', $options['created_until']->format(DateTimeItemInterface::STORAGE_TIMEZONE), '<=');
    }

    if($options['items_per_page'] != null) {
      if($options['page'] != null) {
        $query->range($options['page']*$options['items_per_page'], $options['items_per_page']);
      } else {
        $query->range(0, $options['items_per_page']);
      }
    }
  }

  /**
   * @param $nids
   *
   * @param $options
   *
   * @return array
   */
  public static function ComplaintGetAllDataProcess($nids, $options) {
    $data = array();

    if($nids == null){
      return null;
    }

    foreach ($nids as $nid) {
      $complaint = self::ComplaintNodeFormat($nid);

      if($options['missed_deadline'] != null) {
        if($options['missed_deadline'] == true) {
          if($complaint['field_deadline'] != '') {
            if($complaint['field_resolve_time'] != '') {
              if($complaint['field_resolve_time'] > $complaint['field_deadline']) {
                $data[] = $complaint;
              }
            } else {
              if(date('Y-m-d H:i:s') > $complaint['field_deadline']) {
                $data[] = $complaint;
              }
            }
          }
        } else {
          if($complaint['field_deadline'] != '') {
            if($complaint['field_resolve_time'] != '') {
              if($complaint['field_resolve_time'] < $complaint['field_deadline']) {
                $data[] = $complaint;
              }
            } else {
              if(date('Y-m-d H:i:s') < $complaint['field_deadline']) {
                $data[] = $complaint;
              }
            }
          } else {
            $data[] = $complaint;
          }
        }
      } else {
        $data[] = $complaint;
      }
    }

    if($options['count'] != null) {
      return array('count' => count($data));
    } else {
      return $data;
    }
  }

  /**
   * @param $nid
   *
   * @return array
   */
  public static function ComplaintNodeFormat($nid){
    $node = Node::load($nid)->toArray();

    $tmp_data = array();

    $tmp_resolve_time = date('Y-m-d H:i:s', strtotime($node['field_resolve_time'][0]['value']));
    $tmp_resolve_time = ($tmp_resolve_time == date('Y-m-d H:i:s', null) ? '' : $tmp_resolve_time);

    $tmp_deadline = date('Y-m-d H:i:s', strtotime($node['field_deadline'][0]['value']));
    $tmp_deadline = ($tmp_deadline == date('Y-m-d H:i:s', null) ? '' : $tmp_deadline);

    $tmp_data['nid'] = $nid;
    $tmp_data['title'] = $node['title'][0]['value'];
    $tmp_data['author'] = self::ComplaintUserFormat($node['uid'][0]['target_id']);
    $tmp_data['created'] = date('Y-m-d H:i:s', $node['created'][0]['value']);
    $tmp_data['field_category'] = self::ComplaintTaxonomyFormatMultiple($node['field_category']);
    $tmp_data['field_department'] = self::ComplaintTaxonomyFormat($node['field_department'][0]['target_id']);
    $tmp_data['field_content'] = $node['field_content'][0]['value'];
    $tmp_data['field_deadline'] = $tmp_deadline;
    $tmp_data['field_priority'] = $node['field_priority'][0]['value'];
    $tmp_data['field_resolve_time'] = $tmp_resolve_time;
    $tmp_data['field_status'] = self::ComplaintTaxonomyFormat($node['field_status'][0]['target_id']);
    $tmp_data['field_reject_reason'] = $node['field_reject_reason'][0]['value'];
    $tmp_data['field_technician'] = self::ComplaintUserFormatMultiple($node['field_technician']);

    return $tmp_data;
  }

  /**
   * @param $uid
   *
   * @return array
   */
  public static function ComplaintUserFormat($uid) {
    if($uid == null) {
      return array();
    }
    $df_user = User::load($uid);
    if($df_user == null) {
      return array();
    }
    $df_user = $df_user->toArray();

    $user['id'] = $df_user['uid'][0]['value'];
    $user['name'] = $df_user['name'][0]['value'];

    return $user;
  }

  /**
   * @param $tid
   *
   * @return array
   */
  public static function ComplaintTaxonomyFormat($tid) {
    if($tid == null){
      return array();
    }
    $df_taxonomy = Term::load($tid);
    if($df_taxonomy == null) {
      return array();
    }
    $df_taxonomy = $df_taxonomy->toArray();

    $taxonomy['id'] = $df_taxonomy['tid'][0]['value'];
    $taxonomy['name'] = $df_taxonomy['name'][0]['value'];

    return $taxonomy;
  }

  /**
   * @param $uids
   *
   * @return array
   */
  public static function ComplaintUserFormatMultiple($uids) {
    $user = array();

    foreach ($uids as $uid) {
      $real_uid = $uid['target_id'];
      $tmp_user = self::ComplaintUserFormat($real_uid);
      if($tmp_user != null){
        $user[] = $tmp_user;
      }
    }

    return $user;
  }

  /**
   * @param $tids
   *
   * @return array
   */
  public static function ComplaintTaxonomyFormatMultiple($tids) {
    $taxonomy = array();

    foreach ($tids as $tid) {
      $real_tid = $tid['target_id'];
      $tmp_taxonomy = self::ComplaintTaxonomyFormat($real_tid);
      if($tmp_taxonomy != null){
        $taxonomy[] = $tmp_taxonomy;
      }
    }

    return $taxonomy;
  }

  /**
   * @param $options
   *
   * @return array
   */
  public static function ComplaintGetCountByPriority($options = array()) {
    $data = array();


    for ($i=1; $i<=3; $i++) {
      $query = Drupal::entityQuery('node')
        ->condition('type', 'complaint', '=');

      if($options['status'] != null) {
        $query->condition('field_status', $options['status'], 'IN');
      }

      $query->condition('field_priority', $i, '=');

      $count = $query->count()->execute();

      $data[] = array('priority' => $i, 'count' => $count);
    }

    return $data;
  }

  /**
   * @return array
   */
  public static function ComplaintGetCountByStatus() {
    $data = array();

    for ($i=1; $i<=5; $i++) {
      $query = Drupal::entityQuery('node')
        ->condition('type', 'complaint', '=')
        ->condition('field_status', $i, '=');

      $count = $query->count()->execute();

      $data[] = array('status' => $i, 'count' => $count);
    }

    return $data;
  }
}

