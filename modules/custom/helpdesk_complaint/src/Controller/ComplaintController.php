<?php

namespace Drupal\helpdesk_complaint\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\helpdesk_complaint\Repository\ComplaintRepo;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines ComplaintController class.
 */
class ComplaintController extends ControllerBase {

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return all complaints in json format.
   * @throws \Exception
   */
  public function GetAll() {
    $options = array();
    $options['status'] = Drupal::request()->query->get('status');
    $options['category'] = Drupal::request()->query->get('category');
    $options['technician'] = Drupal::request()->query->get('technician');
    $options['priority'] = Drupal::request()->query->get('priority');
    $options['department'] = Drupal::request()->query->get('department');
    $options['created_from'] = Drupal::request()->query->get('created_from');
    $options['created_until'] = Drupal::request()->query->get('created_until');
    $options['items_per_page'] = Drupal::request()->query->get('items_per_page');
    $options['page'] = Drupal::request()->query->get('page');
    $options['count'] = Drupal::request()->query->get('count');
    $options['missed_deadline'] = Drupal::request()->query->get('missed_deadline');

    $result = ComplaintRepo::ComplaintGetAll($options);
    if($result != null) {
      return new JsonResponse($result);
    } else {
      return new JsonResponse(array(), 204);
    }
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function GetPriorityCount() {
    return new JsonResponse(ComplaintRepo::ComplaintGetCountByPriority());
  }

  /**
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function GetStatusCount() {
    return new JsonResponse(ComplaintRepo::ComplaintGetCountByStatus());
  }

}
