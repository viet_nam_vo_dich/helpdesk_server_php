<?php

namespace Drupal\helpdesk_article\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\helpdesk_article\Repository\ArticleRepo;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines ArticleController class.
 */
class ArticleController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse Return markup array.
   *   Return markup array.
   */
  public function GetCount() {
    return new JsonResponse(ArticleRepo::GetAllArticleCount());
  }

}
