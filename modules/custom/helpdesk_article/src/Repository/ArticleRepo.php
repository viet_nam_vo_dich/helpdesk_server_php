<?php

namespace Drupal\helpdesk_article\Repository;

use Drupal;

trait ArticleRepo {
  static function GetAllArticleCount() {
    $query = Drupal::entityQuery('node')
      ->condition('type', 'article', '=')
      ->sort('nid', 'DESC');

    $count = $query->count()->execute();

    return array('count' => $count);
  }
}
