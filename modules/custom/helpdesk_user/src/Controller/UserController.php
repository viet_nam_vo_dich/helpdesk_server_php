<?php

namespace Drupal\helpdesk_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal;
use Drupal\helpdesk_user\Repository\UserRepo;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines UserController class.
 */
class UserController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse Return markup array.
   *   Return markup array.
   */
  public function GetCount() {
    $param_status = Drupal::request()->query->get('status');

    return new JsonResponse(UserRepo::GetAllUserCount(array('status' => $param_status)));
  }
}
