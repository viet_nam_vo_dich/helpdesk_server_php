<?php

namespace Drupal\helpdesk_user\Repository;

use Drupal;

trait UserRepo {
  static function GetAllUserCount($options = array()) {
    $query = Drupal::entityQuery('user')
      ->condition('uid', 0, '!=')
      ->condition('uid', 1, '!=')
      ->sort('uid', 'DESC');

    if($options['status'] != null) {
      $query->condition('status', $options['status'], 'IN');
    }

    $count = $query->count()->execute();

    return array('count' => $count);
  }
}
