<?php

namespace Drupal\helpdesk_client_dashboard\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal;
use Drupal\helpdesk_user\Repository\UserRepo;
use Drupal\helpdesk_complaint\Repository\ComplaintRepo;
use Drupal\helpdesk_article\Repository\ArticleRepo;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines DashboardController class.
 */
class DashboardController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse Return markup array.
   *   Return markup array.
   * @throws \Exception
   */
  public function Content() {
    $data = array();

    $data['active_complaints'] = ComplaintRepo::ComplaintGetAll(array('count' => true, 'status' => array(1, 3, 5)));
    $data['inactive_complaints'] = ComplaintRepo::ComplaintGetAll(array('count' => true, 'status' => array(2, 4)));
    $data['complaint_status_count'] = ComplaintRepo::ComplaintGetCountByStatus();
    $data['complaint_priority_count'] = ComplaintRepo::ComplaintGetCountByPriority(array('status' => array(1, 3, 5)));

    $now = date('Y-m');
    for($x = 11; $x >= 0; $x--) {
      $date = date('Y-m', strtotime($now . " -$x month"));
      $from = date('Y-m-01\T00:00:00', strtotime($date));
      $until = date('Y-m-t\T23:59:59', strtotime($date));
      $missed = ComplaintRepo::ComplaintGetAll(
        array(
          'created_from' => $from,
          'created_until' => $until,
          'missed_deadline' => true,
          'count' => true
        )
      );

      $in_time = ComplaintRepo::ComplaintGetAll(
        array(
          'created_from' => $from,
          'created_until' => $until,
          'missed_deadline' => '0',
          'count' => true
        )
      );

      $complaint_data = array('missed' => ($missed == null ? "0" : $missed['count'].''), 'in_time' => ($in_time == null ? "0" : $in_time['count'].''), 'date' => $date);

      $data['deadline_complaint_report'][] = $complaint_data;
    }


    $data['active_users'] = UserRepo::GetAllUserCount(1);

    $data['articles'] = ArticleRepo::GetAllArticleCount();

    return new JsonResponse($data);
  }
}
